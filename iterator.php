<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class MyIterator implements Iterator
{
    private $var = array();

    public function __construct($file)
    {
        $this->var = file($file);
    }

    public function rewind()
    {
        reset($this->var);
    }

    public function current()
    {
        $var = current($this->var);
        echo "<pre>";
        print_r(explode(',',$var));
        echo "</pre>";
        return $var;
    }

    public function key()
    {
        $var = key($this->var);
        return $var;
    }

    public function next()
    {
        $var = next($this->var);
        return $var;
    }

    public function valid()
    {
        $key = key($this->var);
        $var = ($key !== NULL && $key !== FALSE);
        return $var;
    }

}

$csv = new MyIterator('cats.csv');

foreach ($csv as $key => $row) {
    print_r($row);
}
