<?php
trait Trait1{
    public function method(){
        return 1;
    }
}

trait Trait2{
    public function method(){
        return 2;
    }
}

trait Trait3{
    public function method(){
        return 3;
    }
}

class Test{
    use Trait1,Trait2,Trait3{
        Trait3::method insteadof Trait1;
        Trait3::method insteadof Trait2;
        Trait1::method as tmethod1;
        Trait2::method as tmethod2;
        Trait3::method as tmethod3;
    }

    public function getSum(){
        return $this->tmethod1()+$this->tmethod2()+$this->tmethod3();
    }
}

$test = new Test();
echo $test->getSum();