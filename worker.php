<?php

class Worker {
    public $name;
    public $age;
    public $salary;
}

$john = new Worker;
$john->name = 'John';
$john->age = 25;
$john->salary = 1000;

$vasia = new Worker;
$vasia->name = 'Вася';
$vasia->age = 26;
$vasia->salary = 2000;

echo $john->salary + $vasia->salary . '<br/>';
echo $john->age + $vasia->age;